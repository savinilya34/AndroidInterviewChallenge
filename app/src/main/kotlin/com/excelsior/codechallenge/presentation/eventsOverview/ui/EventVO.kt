package com.excelsior.codechallenge.presentation.eventsOverview.ui


data class EventVO(
    val id: String,
    val name: String,
    val price: Double,
    val formattedDate: String,
)